
package pluginparticipabr;

import java.util.ArrayList;

public class Cidadao extends Usuario{
    
    private ArrayList <Post> listaPosts;
    private ArrayList <Comentario> listaComentarios;
 
    public String criarPost(Post post) {
        listaPosts.add(post);
        return "Aguarde permissão do Moderador para criar o post.";
        /*Se o Moderador negar, uma mensagem retornaria ao cidadão informando
        que seu Post não teve a permissão para ser criado.*/
    }
    
    public String criarComentario(Comentario comentario) {
        listaComentarios.add(comentario);
        return "Obrigado por expôr aqui a sua opinião!";
    }
}
