
package pluginparticipabr;

import java.util.ArrayList;

public class Moderador extends Usuario{
    
    private ArrayList <Post> listaPosts;
    
    public void permitirCriacaoDePost(Post post) {
        /*Com esta função o Moderado permitiria a criação de um Post solicitada
        por um cidadão.*/
        listaPosts.add(post);
    }
    
    public void avaliarPost() {
        /*Essa função daria a oportunidade ao moderador de dar seu reconhecimento
        ao post de um cidadão.*/
    }
    
    public String excluirPost(Post post) {
        /*Caso o Moderador não queria aceitar a criação de um post por um cidadão
        qualquer, essa função daria a condição de negar a criação do mesmo. Essa
        opção foi criada tendo como base o fato de que muitos usuário poderiam
        utilizar a oportunidade de se criar um post com intenções divergentes
        daquela que o site pretende passar, ajudar a população a melhorar o funcionamento
        de todo um país.*/
        listaPosts.remove(post);
        return "Post excluído com Sucesso!";
    }
    
    public String criarPost(Post post) {
        listaPosts.add(post);
        return "Post criado com Sucesso!";
    }
}
