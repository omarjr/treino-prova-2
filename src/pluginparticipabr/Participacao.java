
package pluginparticipabr;

public class Participacao {
    protected String titulo;

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }
    
    public Participacao(String titulo){
        this.titulo = titulo;
    }
    
}
