
package pluginparticipabr;

import java.util.ArrayList;

public class Post extends Participacao{
    private String descricao;
    private String objetivo;
    private ArrayList<Comentario> listaComentarios;

    public Post(String titulo, String descricao){
        super(titulo);
        this.descricao = descricao;
    }

    public ArrayList<Comentario> getListaComentarios() {
        return listaComentarios;
    }

    public void setListaComentarios(ArrayList<Comentario> listaComentarios) {
        this.listaComentarios = listaComentarios;
    }

   
    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public String getObjetivo() {
        return objetivo;
    }

    public void setObjetivo(String objetivo) {
        this.objetivo = objetivo;
    }

}
