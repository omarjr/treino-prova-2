
package pluginparticipabr;

class Comentario extends Participacao{
    private String resposta;

    public Comentario(String titulo, String resposta) {
        super(titulo);
        this.resposta = resposta;
    }

    public String getResposta() {
        return resposta;
    }

    public void setResposta(String resposta) {
        this.resposta = resposta;
    }
    
}
